#include <iostream>  // diag

#include "Robowaifu.h"

Robowaifu::Robowaifu()
    : head_{std::make_unique<Head>()}, torso_{std::make_unique<Torso>()} {
  // add a pair of arms
  for (unsigned int i = 0; i < 2; ++i)
    arms_.emplace_back(std::make_unique<Arm>());

  // add a pair of legs
  for (unsigned int i = 0; i < 2; ++i)
    legs_.emplace_back(std::make_unique<Leg>());

  std::cout << "\tRobowaifu ctor()\n";  // diag
}
