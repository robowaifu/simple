#pragma once

#include <memory>

#include "Areola.h"
#include "Duct.h"
#include "Nipple.h"

class Breast {
 public:
  Breast();

 private:
  std::unique_ptr<Areola> areola_;
  std::unique_ptr<Nipple> nipple_;
  std::unique_ptr<Duct> duct_;
};