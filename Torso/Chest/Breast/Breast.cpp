#include <iostream>  // diag

#include "Breast.h"

Breast::Breast()
    : areola_{std::make_unique<Areola>()},
      nipple_{std::make_unique<Nipple>()},
      duct_{std::make_unique<Duct>()} {
  std::cout << "\tBreast ctor()\n";  // diag
}
