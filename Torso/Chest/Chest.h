#pragma once

#include <memory>
#include <vector>

#include "Breast/Breast.h"

class Chest {
 public:
  Chest();

 private:
  std::vector<std::unique_ptr<Breast>> breasts_;
};
