#pragma once

#include <memory>

#include "Back/Back.h"
#include "Belly.h"
#include "Chest/Chest.h"
#include "Pelvis/Pelvis.h"

class Torso {
 public:
  Torso();

 private:
  std::unique_ptr<Chest> chest_;
  std::unique_ptr<Belly> belly_;
  std::unique_ptr<Pelvis> pelvis_;
  std::unique_ptr<Back> back_;
};
