#include <iostream>  // diag

#include "Torso.h"

Torso::Torso()
    : chest_{std::make_unique<Chest>()},
      belly_{std::make_unique<Belly>()},
      pelvis_{std::make_unique<Pelvis>()},
      back_{std::make_unique<Back>()} {
  std::cout << "\tTorso ctor()\n";  // diag
}
