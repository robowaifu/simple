#pragma once

#include <memory>
#include <vector>

#include "Canal.h"
#include "Cervix.h"
#include "Clitoris.h"
#include "Labia_major.h"
#include "Labia_minor.h"
#include "Opening.h"
#include "Sheath.h"
#include "Urethra.h"
#include "Vulva.h"

class Vagina {
 public:
  Vagina();

 private:
  std::unique_ptr<Vulva> vulva_;
  std::unique_ptr<Sheath> sheath_;
  std::unique_ptr<Clitoris> clitoris_;
  std::unique_ptr<Urethra> urethra_;
  std::unique_ptr<Opening> opening_;
  std::unique_ptr<Canal> canal_;
  std::unique_ptr<Cervix> cervix_;
  std::vector<std::unique_ptr<Labia_major>> big_lips_;
  std::vector<std::unique_ptr<Labia_minor>> little_lips_;
};
