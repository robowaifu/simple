#include <iostream>  // diag

#include "Vagina.h"

Vagina::Vagina()
    : vulva_{std::make_unique<Vulva>()},
      sheath_{std::make_unique<Sheath>()},
      clitoris_{std::make_unique<Clitoris>()},
      urethra_{std::make_unique<Urethra>()},
      opening_{std::make_unique<Opening>()},
      canal_{std::make_unique<Canal>()},
      cervix_{std::make_unique<Cervix>()} {
  // add a pair of big lips
  for (unsigned int i = 0; i < 2; ++i)
    big_lips_.emplace_back(std::make_unique<Labia_major>());

  // add a pair of little lips
  for (unsigned int i = 0; i < 2; ++i)
    little_lips_.emplace_back(std::make_unique<Labia_minor>());

  std::cout << "\tVagina ctor()\n";  // diag
}
