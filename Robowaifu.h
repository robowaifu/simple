#pragma once

#include <memory>
#include <vector>

#include "Arm/Arm.h"
#include "Head/Head.h"
#include "Leg/Leg.h"
#include "Torso/Torso.h"

class Robowaifu {
 public:
  Robowaifu();

 private:
  std::unique_ptr<Head> head_;
  std::unique_ptr<Torso> torso_;
  std::vector<std::unique_ptr<Arm>> arms_;
  std::vector<std::unique_ptr<Leg>> legs_;
};
