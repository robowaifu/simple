#pragma once

#include <memory>
#include <vector>

#include "Ear.h"
#include "Face/Face.h"
#include "Hair.h"
#include "Jaw.h"
#include "Neck.h"

class Head {
 public:
  Head();

 private:
  std::unique_ptr<Hair> hair_;
  std::unique_ptr<Face> face_;
  std::unique_ptr<Jaw> jaw_;
  std::unique_ptr<Neck> neck_;
  std::vector<std::unique_ptr<Ear>> ears_;
};
