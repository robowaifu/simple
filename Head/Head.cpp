#include <iostream>  // diag

#include "Head.h"

Head::Head()
    : hair_{std::make_unique<Hair>()},
      face_{std::make_unique<Face>()},
      jaw_{std::make_unique<Jaw>()},
      neck_{std::make_unique<Neck>()} {
  // Add a pair of ears
  // There's probably a more sophisticated way to do this during construction
  // using modern C++. Pull requests welcome. :^)
  for (unsigned int i = 0; i < 2; ++i)
    ears_.emplace_back(std::make_unique<Ear>());

  std::cout << "\tHead ctor()\n";  // diag
}
