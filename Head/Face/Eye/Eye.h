#pragma once

#include <memory>
#include <vector>

#include "Eyeball.h"
#include "Eyebrow.h"
#include "Eyelid.h"
#include "Iris.h"

class Eye {
 public:
  Eye();

 private:
  std::unique_ptr<Eyeball> eyeball_;
  std::unique_ptr<Iris> iris_;
  std::unique_ptr<Eyebrow> eyebrow_;
  std::vector<std::unique_ptr<Eyelid>> eyelids_;
};
