#include <iostream>  // diag

#include "Eye.h"

Eye::Eye()
    : eyeball_{std::make_unique<Eyeball>()},
      iris_{std::make_unique<Iris>()},
      eyebrow_{std::make_unique<Eyebrow>()} {
  // add a pair of eyelids
  for (unsigned int i = 0; i < 2; ++i)
    eyelids_.emplace_back(std::make_unique<Eyelid>());

  std::cout << "\tEye ctor()\n";  // diag
}
