#pragma once

#include <memory>
#include <vector>

#include "Lip.h"
#include "Throat.h"
#include "Tongue.h"
#include "Tooth.h"

class Mouth {
 public:
  Mouth();

 private:
  std::unique_ptr<Tongue> tongue_;
  std::unique_ptr<Throat> throat_;
  std::vector<std::unique_ptr<Lip>> lips_;
  std::vector<std::unique_ptr<Tooth>> teeth_;
};
