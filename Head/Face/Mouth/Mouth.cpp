#include <iostream>  // diag

#include "Mouth.h"

Mouth::Mouth()
    : tongue_{std::make_unique<Tongue>()}, throat_{std::make_unique<Throat>()} {
  // add a pair of lips
  for (unsigned int i = 0; i < 2; ++i)
    lips_.emplace_back(std::make_unique<Lip>());

  // add adult teeth count
  for (unsigned int i = 0; i < 32; ++i)
    teeth_.emplace_back(std::make_unique<Tooth>());

  std::cout << "\tMouth ctor()\n";  // diag
}
