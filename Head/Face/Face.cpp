#include <iostream>  // diag

#include "Face.h"

Face::Face()
    : nose_{std::make_unique<Nose>()},
      mouth_{std::make_unique<Mouth>()},
      chin_{std::make_unique<Chin>()} {
  // Add a pair of eyes
  // There's probably a more sophisticated way to do this during construction
  // using modern C++. Pull requests welcome. :^)
  for (unsigned int i = 0; i < 2; ++i)
    eyes_.emplace_back(std::make_unique<Eye>());

  std::cout << "\tFace ctor()\n";  // diag
}
