#pragma once

#include <memory>
#include <vector>

#include "Chin.h"
#include "Eye/Eye.h"
#include "Mouth/Mouth.h"
#include "Nose.h"

class Face {
 public:
  Face();

 private:
  std::unique_ptr<Nose> nose_;
  std::unique_ptr<Mouth> mouth_;
  std::unique_ptr<Chin> chin_;
  std::vector<std::unique_ptr<Eye>> eyes_;
};
