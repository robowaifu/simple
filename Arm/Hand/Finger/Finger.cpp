#include <iostream>  // diag

#include "Finger.h"

Finger::Finger() : nail_{std::make_unique<Nail>()} {
  // add 3 knuckles
  for (unsigned int i = 0; i < 3; ++i)
    knuckles_.emplace_back(std::make_unique<Knuckle>());

  std::cout << "\tFinger ctor()\n";  // diag
}
