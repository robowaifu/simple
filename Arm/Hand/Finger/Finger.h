#pragma once

#include <memory>
#include <vector>

#include "Knuckle.h"
#include "Nail.h"

class Finger {
 public:
  Finger();

 private:
  std::unique_ptr<Nail> nail_;
  std::vector<std::unique_ptr<Knuckle>> knuckles_;
};
