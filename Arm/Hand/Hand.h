#pragma once

#include <memory>
#include <vector>

#include "Finger/Finger.h"
#include "Palm.h"
#include "Wrist.h"

class Hand {
 public:
  Hand();

 private:
  std::unique_ptr<Wrist> wrist_;
  std::unique_ptr<Palm> palm_;
  std::vector<std::unique_ptr<Finger>> fingers_;
};
