#include <iostream>  // diag

#include "Hand.h"

Hand::Hand()
    : wrist_{std::make_unique<Wrist>()}, palm_{std::make_unique<Palm>()} {
  // add 5 fingers
  for (unsigned int i = 0; i < 5; ++i)
    fingers_.emplace_back(std::make_unique<Finger>());

  std::cout << "\tHand ctor()\n";  // diag
}
