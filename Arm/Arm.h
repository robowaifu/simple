#pragma once

#include <memory>
#include <vector>

#include "Elbow.h"
#include "Hand/Hand.h"
#include "Lower_arm.h"
#include "Shoulder.h"
#include "Upper_arm.h"

class Arm {
 public:
  Arm();

 private:
  std::unique_ptr<Shoulder> shoulder_;
  std::unique_ptr<Upper_arm> upper_;
  std::unique_ptr<Elbow> elbow_;
  std::unique_ptr<Lower_arm> lower_;
  std::unique_ptr<Hand> hand_;
};
