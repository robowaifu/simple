#include <iostream>  // diag

#include "Arm.h"

Arm::Arm()
    : shoulder_{std::make_unique<Shoulder>()},
      upper_{std::make_unique<Upper_arm>()},
      elbow_{std::make_unique<Elbow>()},
      lower_{std::make_unique<Lower_arm>()},
      hand_{std::make_unique<Hand>()} {
  std::cout << "\tArm ctor()\n";  // diag
}