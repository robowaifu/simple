#pragma once

#include <memory>
#include <vector>

#include "Knuckle_t.h"
#include "Nail_t.h"

class Toe {
 public:
  Toe();

 private:
  std::unique_ptr<Nail_t> nail_;
  std::vector<std::unique_ptr<Knuckle_t>> knuckles_;
};
