#include <iostream>  // diag

#include "Toe.h"

Toe::Toe() : nail_{std::make_unique<Nail_t>()} {
  // add 3 knuckles
  for (unsigned int i = 0; i < 3; ++i)
    knuckles_.emplace_back(std::make_unique<Knuckle_t>());

  std::cout << "\tToe ctor()\n";  // diag
}
