#pragma once

#include <memory>
#include <vector>

#include "Ankle.h"
#include "Arch.h"
#include "Heel.h"
#include "Toe/Toe.h"

class Foot {
 public:
  Foot();

 private:
  std::unique_ptr<Ankle> ankle_;
  std::unique_ptr<Arch> arch_;
  std::unique_ptr<Heel> heel_;
  std::vector<std::unique_ptr<Toe>> toes_;
};
