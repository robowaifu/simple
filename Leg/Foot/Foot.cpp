#include <iostream>  // diag

#include "Foot.h"

Foot::Foot()
    : ankle_{std::make_unique<Ankle>()},
      arch_{std::make_unique<Arch>()},
      heel_{std::make_unique<Heel>()} {
  // add 5 toes
  for (unsigned int i = 0; i < 5; ++i)
    toes_.emplace_back(std::make_unique<Toe>());

  std::cout << "\tFoot ctor()\n";  // diag
}
