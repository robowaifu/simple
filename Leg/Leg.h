#pragma once

#include <memory>

#include "Foot/Foot.h"
#include "Hip.h"
#include "Knee.h"
#include "Lower_leg.h"
#include "Upper_leg.h"

class Leg {
 public:
  Leg();

 private:
  std::unique_ptr<Hip> hip_;
  std::unique_ptr<Upper_leg> upper_;
  std::unique_ptr<Knee> knee_;
  std::unique_ptr<Lower_leg> lower_;
  std::unique_ptr<Foot> foot_;
};
