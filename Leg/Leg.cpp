#include <iostream>  // diag

#include "Leg.h"

Leg::Leg()
    : hip_{std::make_unique<Hip>()},
      upper_{std::make_unique<Upper_leg>()},
      knee_{std::make_unique<Knee>()},
      lower_{std::make_unique<Lower_leg>()},
      foot_{std::make_unique<Foot>()} {
  std::cout << "\tLeg ctor()\n";  // diag
}
