# simple


Simple hierarchical description for a Robowaifu externally-visible shell. This
can serve as a basis for expanding into a full technical control description
(ie, GPIO pin addresses, servo control values and code, &tc), a machine-learning
system, a kinematics simulation system, a vidya or a Visual Waifu (including
VR/AR), or even a full-blown Robowaifu.
